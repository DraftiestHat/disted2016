package disted2016;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ClassroomEvent {
	private String courseName = null;
	private long id = 0;
	private String professor = null;
	private String room = null;
	private long startTime = 0;
	private long endTime = 0;
	private StatEnum status;
	private boolean offlineFileNeeded;
	private boolean changed = false;
	
	public ClassroomEvent(String courseName, long id, String professor,
			String room, Calendar startTime, Calendar endTime, StatEnum status,
			boolean offlineFileNeeded) {
		this.courseName = courseName;
		this.id = id;
		this.professor = professor;
		this.room = room;
		this.startTime = startTime.getTimeInMillis();
		this.endTime = endTime.getTimeInMillis();
		this.status = status;
		this.offlineFileNeeded = offlineFileNeeded;
	}
	
	public ClassroomEvent(String coursename, String id, String professor,String room, String startTime, String endTime,String status,String offlineFileNeeded){
		
		//SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.EN_US);


		int enumstat = Integer.parseInt(status);
		
		
		
		this.courseName = coursename;
		this.id = Integer.parseInt(id);
		this.professor = professor;
		this.room = room;
		this.startTime = Long.parseLong(startTime);
		this.endTime = Long.parseLong(endTime);
		
		if(enumstat == 1){
			this.status = StatEnum.FUTURE;
		}
		else if(enumstat ==2){
			this.status = StatEnum.RECORDING ;
		}
		else if(enumstat ==3){
			this.status = StatEnum.RECORDED;
		}
		else if(enumstat ==4){
			this.status = StatEnum.DOWNLOADED;
		}
		else if(enumstat ==5){
			this.status = StatEnum.NO_DOWNLOAD_NEEDED;
		}
		this.offlineFileNeeded = Boolean.parseBoolean(offlineFileNeeded);
		
		
	}
	
	public ClassroomEvent(long id, Calendar startTime, Calendar endTime,
			StatEnum status, boolean offlineFileNeeded)	{
		this(null, id, null, null, startTime, endTime, status, offlineFileNeeded);
	}
	
	@Override
	public boolean equals(Object other) {
		if(!(other instanceof ClassroomEvent)) {
			return false;
		}
		
		ClassroomEvent ev = (ClassroomEvent) other;
		
		if(this.id == ev.id){
			if(this.startTime == ev.startTime){
				return true;
			}
		}
		
		return false;
		
	}
	public Date getDate(){
		
		java.sql.Date sqlDate = new java.sql.Date(this.getStartLong());
		return sqlDate;
	}
	
	public Long getStartLong(){
		
		return startTime;
	}
	public Long getEndLong(){
		
		return endTime;
	}
	public String getCourseName() {
		return courseName;
	}
	
	public void setCourseName(String courseName) {
		this.courseName = courseName;
		this.changed = true;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
		this.changed = true;
	}
	
	public String getProfessor() {
		return professor;
	}
	
	public void setProfessor(String professor) {
		this.professor = professor;
		this.changed = true;
	}
	
	public String getRoom() {
		return room;
	}
	
	public void setRoom(String room) {
		this.room = room;
		this.changed = true;
	}
	
	public Calendar getStartTime() {
		Calendar x = Calendar.getInstance();
		x.setTimeInMillis(this.startTime);
		return x;
	}
	
	public void setStartTime(Calendar startTime) {
		this.startTime = startTime.getTimeInMillis();
		this.changed = true;
	}
	
	public Calendar getEndTime() {
		Calendar x = Calendar.getInstance();
		x.setTimeInMillis(this.endTime);
		return x;
	}
	
	public void setEndTime(Calendar endTime) {
		this.endTime = endTime.getTimeInMillis();
		this.changed = true;
	}
	
	public StatEnum getStatus() {
		return status;
	}
	
	public void setStatus(StatEnum status) {
		this.status = status;
		this.changed = true;
	}
	
	public boolean isOfflineFileNeeded() {
		return offlineFileNeeded;
	}
	
	public void setOfflineFileNeeded(boolean offlineFileNeeded) {
		this.offlineFileNeeded = offlineFileNeeded;
		this.changed = true;
	}
	
	public boolean isChanged() {
		return changed;
	}

	public void print() {
		System.out.printf(courseName + "; ");
		System.out.printf(id + "; ");
		System.out.printf(professor + "; ");
		System.out.printf(room + "; ");
		System.out.printf(startTime + "; ");
		System.out.printf(endTime + "; ");
		System.out.printf(status + "; ");
		System.out.printf(offlineFileNeeded + "\n");
		
	}
	
	
}
