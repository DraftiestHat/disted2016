package disted2016;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Timer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.UIManager;

public class Main {
	
	public static ServerInterface si;
	public static MainPage window;

	public static void main(String[] args) {
		
		final JFrame frame = new JFrame("Distance Ed Classroom Download Tracker");
		final JButton btnLogin = new JButton("Click to login");
		
		
		btnLogin.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				LoginPage loginPage = new LoginPage(frame);
				loginPage.setVisible(true);
			}
		});
		
		frame.addWindowListener(new WindowAdapter() {
        	public void windowClosing(WindowEvent we){
        		ACLogin.getNotifier().notifyAll();
        		frame.dispose();
        	}
		});
		
        frame.setSize(300, 100);
        frame.setLayout(new FlowLayout());
        frame.getContentPane().add(btnLogin);
        frame.setVisible(true);
		
		
		synchronized (ACLogin.getNotifier()) {
			try {
				ACLogin.getNotifier().wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		 
		
		synchronized (ACLogin.getCookie()) {
			if (ACLogin.getCookie().equals("")) {
				System.exit(0);
			} 
		}
		
		frame.dispose();
		
		//Builds the actual data we will show. Handles ALL communication.
		si = ServerInterface.getServerInterface();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.put("swing.boldMetal", Boolean.FALSE);
					window = new MainPage();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		Timer timer = new Timer();
		timer.schedule(new UpdateStatuses(window), 300000, 300000);
		
		
		
	}

}
