package disted2016;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public final class ServerInterface {
	//for singleton
	private static ServerInterface instance = null;
	
	//stuff to use
	private static MSSQLInterface msi = MSSQLInterface.getInstance();
	
	//holds info
	public static ArrayList<ClassroomEvent> mainEvents = new ArrayList<ClassroomEvent>();
	public static ArrayList<ClassroomEvent> historyEvents = new ArrayList<ClassroomEvent>();
	public static ArrayList<ClassroomEvent> toDoEvents = new ArrayList<ClassroomEvent>();
	
	private ServerInterface()
	{
		;
	}
	
	public static ServerInterface getServerInterface()
	{
		if(instance == null) {
			instance = new ServerInterface();
		}
		
		return instance;
	}

	public static ArrayList<ClassroomEvent> getMainEvents() {
		mainEvents = msi.getEvents();
		return mainEvents;
	}

	public static ArrayList<ClassroomEvent> getHistoryEvents(Date date) {
		historyEvents = msi.getEvents(date);	
		return historyEvents;
	}

	public static ArrayList<ClassroomEvent> getToDoEvents() {
		toDoEvents = msi.getEventsToDo();
		return toDoEvents;
	}
	
	public static void updateDB()
	{
		try {
			ArrayList<ClassroomEvent> x = GoogleCalendarInterface.pullData();
			
			//DELETE ME TO STOP PRINTING WHEN UPDATING
			for(ClassroomEvent i : x) {
				i.print();
			}
			
			msi.pushToDatabase(x);
		} catch (IOException e) {
			// TODO Throw error to user
			e.printStackTrace();
		}
		
	}
	
	
}
