package disted2016;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;

public final class GoogleCalendarInterface {
	
	private static GoogleCalendarInterface instance = null;
	
	private static final String APPLICATION_NAME =
	        "Google Calendar API Java Single User Test";
	
    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY =
        JacksonFactory.getDefaultInstance();

    /** Global instance of the HTTP transport. */
    private static HttpTransport HTTP_TRANSPORT;

    /** Global instance of the scopes required.
     *+
     */
    private static final List<String> SCOPES =
        Arrays.asList(CalendarScopes.CALENDAR_READONLY);

    
    private static com.google.api.services.calendar.Calendar service = null;

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }
    
    /**
     * Creates an authorized Credential object.
     * @return an authorized Credential object.
     * @throws IOException
     */
    public static Credential authorize() throws IOException {
    	GoogleCredential cred = GoogleCredential
    			.fromStream(new FileInputStream("service_secret.json"))
    			.createScoped(SCOPES);

		return cred;
    }
    
    /**
     * Build and return an authorized Calendar client service.
     * @return an authorized Calendar client service
     * @throws IOException
     */
    public static com.google.api.services.calendar.Calendar
        getCalendarService() throws IOException {
        Credential credential = authorize();
        return new com.google.api.services.calendar.Calendar.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }
    
    private GoogleCalendarInterface() {
    	//Do i need to do anything here? IDK yet.
    }
    
    /**
     * Along with the constructor, makes this a singleton class
     * @return The GoogleCalendarInterface for the program
     */
    public static GoogleCalendarInterface getInstance() {
    	if(instance == null) {
    		instance = new GoogleCalendarInterface();
    	}
    	
    	return instance;	
    }
    
    
    public static ArrayList<ClassroomEvent> pullData() throws IOException {
    	String calendarId = null;
    	service = getCalendarService();
        CalendarList l = service.calendarList().list().execute();
        ArrayList<ClassroomEvent> ret = new ArrayList<ClassroomEvent>();
       
        for(CalendarListEntry entry : l.getItems()) {
        	//System.out.printf("%s\n", entry.getSummary());
        	calendarId = entry.getId();
        	
        	// List the next 10 events from the primary calendar.
            DateTime now = new DateTime(System.currentTimeMillis());
            com.google.api.services.calendar.model.Events events =
            	service.events().list(calendarId)
                .setMaxResults(20)
                .setTimeMin(now)
                .setOrderBy("startTime")
                .setSingleEvents(true)
                .execute();
            List<Event> items = events.getItems();
            if (items.size() > 0) {
                //System.out.println("Upcoming events");
                for (Event event : items) {
                    DateTime start = event.getStart().getDateTime();
                    if (start == null) {
                        start = event.getStart().getDate();
                    }
                    //System.out.printf("%s; %s, (%s)\n", event.getSummary(),event.getDescription(), start);
                    ret.add(makeClassroomEvent(event, entry.getSummary()));
                }
                
            }
        	
        }
    	
    	return ret;
    }
    
    private static ClassroomEvent makeClassroomEvent(Event ev, String room) {    	
    	String courseName = null; //
    	long id = 0; //
    	String professor = null; //
    	Calendar startTime = null;
    	Calendar endTime = null;
    	StatEnum status = StatEnum.FUTURE;
    	boolean offlineFileNeeded = true;
    	
    	String[] tmp = ev.getSummary().split("/");
    	
    	if(tmp.length == 3) {
    		courseName = tmp[0] + "/" + tmp[1];
    		professor = tmp[2];
    	} else if(tmp.length == 2) {
    		courseName = tmp[0];
    		professor = tmp[1];
    	}
    	
    	
    	
    	tmp = ev.getDescription().split(",");
    	
    	for (String i : tmp) {
			String[] cur = i.split(":");
			if(cur.length != 2) {
				System.out.println("ISSUE!!! In GoogleCalInterface.");
				System.out.println(i);
				continue;
			}
			
			if(cur[0].toLowerCase().equals("id")) {
				try {
					id = Long.parseLong(cur[1]);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (cur[0].toLowerCase().equals("flag")){
				if (cur[1].matches("y")) {
					offlineFileNeeded = true;
				} else if (cur[1].matches("n")) {
					offlineFileNeeded = false;
					status = StatEnum.NO_DOWNLOAD_NEEDED;
				}
				
			}else {
				System.out.println("unrecognized thing in description: " + i );
			}			
		}
    	
    	DateTime start = ev.getStart().getDateTime();
    	
    	if (start == null) {
            start = ev.getStart().getDate();
        }
    	
    	DateTime end = ev.getEnd().getDateTime();
    	
    	if (end == null) {
            end = ev.getEnd().getDate();
        }
    	
    	startTime = convertDateTime(start);
    	endTime = convertDateTime(end);
    	
    	
    	return new ClassroomEvent(courseName, id, professor,
    			room, startTime, endTime, status, offlineFileNeeded);
    }
    
    @SuppressWarnings("unused")
	private static Calendar convertDateTime(DateTime toConvert) {
    	Calendar c = Calendar.getInstance();
    	
    	Integer shift = toConvert.getTimeZoneShift();
    	
    	if(shift == null)
			shift = new Integer(-7 * 60); // Mountain time zone
    	
    	long secs = toConvert.getValue() + (shift * 60 * 100)+360000;
    	 
    	c.setTime(new Date(secs));
    	
    	return c;
    }
    
    
}
