package disted2016;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import org.jdatepicker.DateModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.JProgressBar;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;


public class MainPage {

	private JFrame frame;
	private JPanel mainTab;
	private JTable mainTable;
	private JPanel historyTab;
	private JTable historyTable;
	private JPanel toDoTab;
	private JTable toDoTable;
	private ArrayList<ClassroomEvent> toDoEvents;
	private ArrayList<ClassroomEvent> historyEvents;
	private ArrayList<ClassroomEvent> mainEvents;
	
	
	private JScrollPane mainScrollPane = null;
	private JScrollPane toDoScrollPane = null;
	private JScrollPane historyScrollPane = null;
	
	private JDatePickerImpl datePicker = null;
	private Date jDatePickerDate = null;
	
	private SimpleDateFormat timeFormat = new SimpleDateFormat("H:mm");
	private TableCellRenderer timeRenderer = new FormatRenderer( timeFormat );
	private SimpleDateFormat dayFormat = new SimpleDateFormat("MM-dd-yyyy");
	private TableCellRenderer dayRenderer = new FormatRenderer( dayFormat );
	public static ServerInterface si;
	
	Object[] mainColumns = {
			"Course Name",
            "ID",
            "Room",
            "Instructor",
            "Time",
            "Status"
    };
		
	Object[] toDoColumns = {
			"Course Name", 
			"Date",
			"Start", 
			"End", 
			"Finished"
	};
	
	Object[] historyColumns = {
		"Course Name",
		"Room",
		"Instructor",
		"Start",
		"Status"		
	};

	/**
	 * Create the application.
	 */
	public MainPage() {
		si = ServerInterface.getServerInterface();
		ServerInterface.updateDB();
		initialize();

		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1200, 900);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		makeMainTab();
		tabbedPane.addTab("Main", null, mainTab, null);
		tabbedPane.setEnabledAt(0, true);
		
		makeHistoryTab();
		tabbedPane.addTab("History", null, historyTab, null);
		tabbedPane.setEnabledAt(1, true);
		
		makeToDoTab();
		tabbedPane.addTab("To Do", null, toDoTab, null);
		tabbedPane.setEnabledAt(2, true);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmUpdateDatabase = new JMenuItem("Update Database");
		mnFile.add(mntmUpdateDatabase);
		mntmUpdateDatabase.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ServerInterface.updateDB();
				//UPDATE TABLES
				updateAllTabs();
			}
		});
		
	}
	
	private void makeMainTab()
	{
		mainTab = new JPanel();
		mainTab.setLayout(new BorderLayout(0, 0));
		
		mainScrollPane = new JScrollPane();
		mainTab.add(mainScrollPane);
		
		makeMainTable();
		
        mainTable.getColumnModel().getColumn(4).setCellRenderer(timeRenderer);
		mainScrollPane.setViewportView(mainTable);
		mainTable.setFillsViewportHeight(true);
	}
	
	private void makeMainTable()
	{
		mainEvents = ServerInterface.getMainEvents();
		DefaultTableModel mainTableModel = new DefaultTableModel(makeMain2DArray(mainEvents), mainColumns){
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int col) {
				if (col == 5) {
		            return true;
		        } else {
		            return false;
		        }       
		    }
		};
		
		mainTable = new JTable(mainTableModel){ 
			private static final long serialVersionUID = 1L;
			
			@Override
	        public Class<?> getColumnClass(int column) {
	            switch (column) {
	                case 0:
                		return String.class;
	                case 1:
	                	return Long.class;
	                case 2:
	                case 3:
	                	return String.class;
	                case 4:
	                	return Date.class;
	                default:
	                    return String.class;
	            }
	        }
        };
	}
	
	private void makeHistoryTab()
	{
		historyTab = new JPanel();
		historyTab.setLayout(new BorderLayout(0, 0));

		JPanel panelDatePicking = new JPanel();
		historyTab.add(panelDatePicking, BorderLayout.NORTH);
		
		//JDatePicker Stuff
		//Almost all from StackOverflow, because i would never have figured this out on my own
		UtilDateModel dateModel = new UtilDateModel();
		
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		JDatePanelImpl datePanel = new JDatePanelImpl(dateModel, p);
		
		datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
        
		//END JDatePicker Stuff
		
		panelDatePicking.add(datePicker);
		
		historyScrollPane = new JScrollPane();
		historyTab.add(historyScrollPane, BorderLayout.CENTER);
		
		makeHistoryTable(new Date());
		
		historyScrollPane.setViewportView(historyTable);
		
		
		datePicker.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Date x = (Date) datePicker.getModel().getValue();
				makeHistoryTable(x);
				historyScrollPane.setViewportView(historyTable);
			}
		});
		
		
		
	}
	
	private void makeHistoryTable(Date date)
	{
		historyEvents = ServerInterface.getHistoryEvents(date);
		DefaultTableModel histModel = new DefaultTableModel(makeHistory2DArray(historyEvents), historyColumns){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			
			@Override
			public boolean isCellEditable(int row, int col) {
				if(col == 4) {
					return true;
				}
				
				return false;
			}
			
		};
		
		historyTable = new JTable(histModel) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			
			/*Object[] historyColumns = {
			"Course Name",
			"Room",
			"Instructor",
			"Start",
			"Status"		
			};*/
			
			@Override
			public Class<?> getColumnClass(int column) {
				switch(column) {
				case 0:
				case 1:
				case 2:
					return String.class;
				case 3:
					return Date.class;
				case 4:
					return String.class;
				default:
					return Object.class;
				}
			}
		};

		historyTable.getColumnModel().getColumn(3).setCellRenderer(timeRenderer);
	}
	
	private void makeToDoTab()
	{
		toDoTab = new JPanel();
		toDoTab.setLayout(new BorderLayout(0, 0));
		
		toDoScrollPane = new JScrollPane();
		toDoTab.add(toDoScrollPane);
		
		this.toDoEvents = ServerInterface.getToDoEvents();
		
		makeToDoTable();
		
		toDoTable.setFillsViewportHeight(true);
		
		TableColumnModel toDoTableModel = toDoTable.getColumnModel();
		toDoTableModel.getColumn(3).setCellRenderer(timeRenderer);
		toDoTableModel.getColumn(2).setCellRenderer(timeRenderer);
		toDoTableModel.getColumn(1).setCellRenderer(dayRenderer);
		
		
		
		toDoScrollPane.setViewportView(toDoTable);
	}
	
	private void makeToDoTable()
	{
		DefaultTableModel tableModel = new DefaultTableModel(makeToDo2DArray(toDoEvents), toDoColumns){
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int col) {
				if (col == 4) {
		            return true;
		        } else {
		            return false;
		        }       
		    }
		};
		
		tableModel.addTableModelListener(new TableModelListener() {
			
			@Override
			public void tableChanged(TableModelEvent e) {
				int row = e.getFirstRow();
				ClassroomEvent ev = toDoEvents.get(row);
				if(ev.getStatus() == StatEnum.RECORDED) {
					ev.setStatus(StatEnum.DOWNLOADED);
				} else {
					ev.setStatus(StatEnum.RECORDED);
				}
				MSSQLInterface.getInstance().updateEventStatus(ev);			
				toDoEvents = ServerInterface.getToDoEvents();
				makeToDoTable();
				toDoScrollPane.setViewportView(toDoTable);
			}
		});
		
		toDoTable = new JTable(tableModel) {
			private static final long serialVersionUID = 1L;
			
			@Override
            public Class<?> getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return String.class;
                    case 1:
                    case 2:
                    case 3:
                        return Date.class;
                    default:
                        return Boolean.class;
                }
            }
		};
	}
	
	private Object[][] makeMain2DArray(ArrayList<ClassroomEvent> input) 
	{
		/*String[] mainColumns = {
				"Course Name",
	            "ID",
	            "Room",
	            "Instructor",
	            "Time",
	            "Status"
	    };*/
		
		Object[][] ret = new Object[input.size()][mainColumns.length];
		
		System.out.println("Object array is supposed to be of size " + input.size() + "x" + mainColumns.length);
		System.out.println("It is " + ret.length + "x" + ret[0].length);
		
		int i = 0;
		
		for(ClassroomEvent ev : input) {
			ret[i][0] = ev.getCourseName();
			ret[i][1] = ev.getId();
			ret[i][2] = ev.getRoom();
			ret[i][3] = ev.getProfessor();
			ret[i][4] = ev.getStartTime().getTime();
			ret[i][5] = ev.getStatus().getName();
			i++;
		}
		
		return ret;
	}
	
	private Object[][] makeHistory2DArray(ArrayList<ClassroomEvent> input) 
	{
		Object[][] ret = new Object[input.size()][historyColumns.length];
		
		int i = 0;
		
		for(ClassroomEvent ev : input) {
			ret[i][0] = ev.getCourseName();
			ret[i][1] = ev.getRoom();
			ret[i][2] = ev.getProfessor();
			ret[i][3] = ev.getStartTime().getTime();
			ret[i][4] = ev.getStatus().getName();
			i++;
		}
		
		return ret;
	}
	
	private Object[][] makeToDo2DArray(ArrayList<ClassroomEvent> input) 
	{
		/*Object[] toDoColumns = {
		"Course Name", 
		"Date",
		"Start", 
		"End", 
		"Finished"
};*/
		Object[][] ret = new Object[input.size()][toDoColumns.length];
		
		int i = 0;
		
		for(ClassroomEvent ev : input) {
			ret[i][0] = ev.getCourseName();
			ret[i][1] = ev.getDate();
			ret[i][2] = ev.getStartTime().getTime();
			ret[i][3] = ev.getEndTime().getTime();
			if(ev.getStatus() == StatEnum.RECORDED) {
				ret[i][4] = false;
			} else {
				ret[i][4] = true;
			}
			i++;
		}
		
		return ret;
		
	}
	
	public void updateEventLists()
	{
		historyEvents = ServerInterface.getHistoryEvents(getJDatePickerDate());
		toDoEvents = ServerInterface.getToDoEvents();
		mainEvents = ServerInterface.getMainEvents();
		
	}
	
	public void updateAllTabs()
	{
		updateEventLists();
		
		makeToDoTable();
		makeHistoryTable(getJDatePickerDate());
		makeMainTable();
		
		mainScrollPane.setViewportView(mainTable);
		toDoScrollPane.setViewportView(toDoTable);
		historyScrollPane.setViewportView(historyTable);		
	}
	
	private Date getJDatePickerDate()
	{
		jDatePickerDate = (Date) datePicker.getModel().getValue();
		return jDatePickerDate;
	}
	
}
