package disted2016;

public enum StatEnum {
	FUTURE(1), RECORDING(2), RECORDED(3), DOWNLOADED(4), NO_DOWNLOAD_NEEDED(5);
	
	private int num;
	
	StatEnum(int num){
		this.num = num;
	}
	
	public int getNumVal() {
		return this.num;
	}
	
	public String getName() {
		switch(num) {
		case 1:
			return "Future";
		case 2:
			return "Recording";
		case 3:
			return "Recorded";
		case 4:
			return "Downloaded";
		case 5:
			return "No Download Needed";
		default:
			return "Unknown";
		}
	}
}
