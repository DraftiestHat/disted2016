package disted2016;

import java.util.ArrayList;
import java.util.Date;
import java.util.TimerTask;

public class UpdateStatuses extends TimerTask {

	private MainPage window = null;
	
	public UpdateStatuses(MainPage window) {
		this.window = window;
	}

	@Override
	public void run() {
		Date now = new Date();
		ArrayList<ClassroomEvent> modified = new ArrayList<ClassroomEvent>();
		ArrayList<ClassroomEvent> events = ServerInterface.getMainEvents();
		
		for(ClassroomEvent ev : events) {
			if(ev.getStatus().getNumVal() < 3 && ev.getStartTime().getTime().before(now)) {
				if(ev.getEndTime().getTime().after(now)) {
					ev.setStatus(StatEnum.RECORDING);
					modified.add(ev);
				} else {
					ev.setStatus(StatEnum.RECORDED);
					modified.add(ev);
				}
				
			}
		}
		
		MSSQLInterface msi = MSSQLInterface.getInstance();
		
		msi.updateEventsStatus(modified);
		
		window.updateAllTabs();
		
	}

}
