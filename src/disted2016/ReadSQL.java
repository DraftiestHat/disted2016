package disted2016;

import java.sql.*;

public class ReadSQL {

   public static void main(String[] args) {

     
      String connectionUrl = "jdbc:sqlserver://129.138.23.50:1433;" +
         "databaseName=classviewdb;user=sa;password=password";

      
      Connection con = null;
      Statement stmt = null;
      ResultSet rs = null;

      try {
      
         Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
         con = DriverManager.getConnection(connectionUrl);

         
         String SQL = "SELECT TOP 5 * FROM dbo.main";
         stmt = con.createStatement();
         rs = stmt.executeQuery(SQL);

         
         while (rs.next()) {
            System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4) + " "  + rs.getString(5)+ " " +  rs.getString(6) + " " + rs.getString(7)+ " "+ rs.getString(8));
         }
      }

      // Handle any errors that may have occurred.
      catch (Exception e) {
         e.printStackTrace();
      }
      finally {
         if (rs != null) try { rs.close(); } catch(Exception e) {}
         if (stmt != null) try { stmt.close(); } catch(Exception e) {}
         if (con != null) try { con.close(); } catch(Exception e) {}
      }
   }
}