package disted2016;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

public final class MSSQLInterface {
	
	private static MSSQLInterface instance = null;
	static Connection con = null;
    static Statement stmt = null;
    ResultSet rs = null;
    
	static String connectionUrl = "jdbc:sqlserver://129.138.23.50:1433;" +
	         "databaseName=classviewdb;user=sa;password=password";

	     
	      
	
	
	private MSSQLInterface()
	{
		//TODO: initializing shit
	}
	
	public static MSSQLInterface getInstance(){
		if(instance == null) {
			instance = new MSSQLInterface();
		}
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			con = DriverManager.getConnection(connectionUrl);
			stmt = con.createStatement();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		
		return instance;
	}
	
	/**
	 * Gets the entries for today
	 * @return
	 */
	public ArrayList<ClassroomEvent> getEvents()
	{
		return getEvents(new Date());
	}
	
	/**
	 * Gets the events for a certain date
	 * @param date
	 * @return
	 */
	public ArrayList<ClassroomEvent> getEvents(Date date)
	{
		String sql = null;
		ArrayList<ClassroomEvent> classes = new ArrayList<ClassroomEvent>();
		 ClassroomEvent event = null;
		
		//gets data from the database based on a certain date
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		
		System.out.println(sqlDate.toString());
		
		System.out.println("REACHED and date is " + sqlDate.toString() );
		
		sql = "SELECT CourseName, SCO_ID, PROFESSOR, Room, StartTime, EndTime, Status, OfflineFlag, Date FROM dbo.main" +
                " WHERE Date = '" +sqlDate+ "' ";
		
		try {
			rs = stmt.executeQuery(sql);
			
			
			
			while (rs.next()) {
				event = new ClassroomEvent(rs.getString(1), rs.getString(2), rs.getString(3),
						rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8));
	            System.out.println("RECORD " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4)+ " " + rs.getString(5)+ " " + rs.getString(6)+ " " + rs.getString(7)+ " " + rs.getString(8)+ " " + rs.getString(9));
	            classes.add(event);
	         }
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.println("Return arraylist of size " + classes.size());
		
		return classes;
	}
	public void deleteEvent(ClassroomEvent event){
		//takes an event and deletes all records in SQL 
		
		Time x = new Time(event.getStartLong());
		String signature = event.getDate().toString() +"_"+ x.toString()+ "_"+event.getCourseName();
		
		
		String sql = "DELETE FROM dbo.main" +
                " WHERE Signature = '" +signature+ "' ";
		
		try {
			rs = stmt.executeQuery(sql);
					
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return;
	}
	
	public void deleteEvents(ArrayList<ClassroomEvent> events){
		//takes an array list and deletes all records in SQL that are in array list
		
		for(ClassroomEvent event : events)
		{
			deleteEvent(event);
		}
		
		return;
	}
	
	/**
	 * Gets all the events that need to be done
	 * @return
	 */
	public ArrayList<ClassroomEvent> getEventsToDo()
	{
		//OFFLINE FLAG TRUE AND STATUS IS 3
		String sql = null;
		ArrayList<ClassroomEvent> classes = new ArrayList<ClassroomEvent>();
		 ClassroomEvent event = null;
		
		
		int classStatus = 3;
		System.out.println("REACHED and status is " + classStatus + "with  offline flag being true");
		
		sql = "SELECT CourseName, SCO_ID, PROFESSOR, Room, StartTime, EndTime, Status, OfflineFlag, Date FROM dbo.main" +
                " WHERE Status = '" +classStatus+ "' AND OfflineFlag = '" + 1+"' ";
		
		try {
			rs = stmt.executeQuery(sql);
			
			
			
			while (rs.next()) {
				event = new ClassroomEvent(rs.getString(1), rs.getString(2), rs.getString(3),
						rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8));
	            System.out.println("RECORD " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4)+ " " + rs.getString(5)+ " " + rs.getString(6)+ " " + rs.getString(7)+ " " + rs.getString(8)+ " " + rs.getString(9));
	            classes.add(event);
	         }
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return classes;
	}
	
	public ArrayList<ClassroomEvent> getEventsOnStatus(StatEnum status){
		
		//GET ALL EVENNTS WITH STATUS
		
		String sql = null;
		ArrayList<ClassroomEvent> classes = new ArrayList<ClassroomEvent>();
		 ClassroomEvent event = null;
		
		//gets data from the database based on a certain date
		int classStatus = status.getNumVal();
		System.out.println("REACHED and status is " + classStatus );
		
		sql = "SELECT CourseName, SCO_ID, PROFESSOR, Room, StartTime, EndTime, Status, OfflineFlag, Date FROM dbo.main" +
                " WHERE Status = '" +classStatus+ "' ";
		
		try {
			rs = stmt.executeQuery(sql);
			
			
			
			while (rs.next()) {
				event = new ClassroomEvent(rs.getString(1), rs.getString(2), rs.getString(3),
						rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8));
	            System.out.println("RECORD " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4)+ " " + rs.getString(5)+ " " + rs.getString(6)+ " " + rs.getString(7)+ " " + rs.getString(8)+ " " + rs.getString(9));
	         }
			
			classes.add(event);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return classes;
		
		
	}
	
	/**
	 * Get the next 'x' number of events
	 * @param events
	 */
	public ArrayList<ClassroomEvent> getNextXEvents(int x)
	{
		
		//GET NEXT X EVENTS
		return null;
	}
	
	public void pushToDatabase(ArrayList<ClassroomEvent> events)
	{
		for(ClassroomEvent event : events)
		{
			pushEvent(event);
		}
	}
	
	/**
	 * Take a single event, check if it's in the database by both ID and start time
	 * If it's in the DB, nothing to do, else either push to or change in DB
	 * @param event
	 */
	public void pushEvent(ClassroomEvent event)
	{
		//test with JUnit
		PreparedStatement preparedStatement = null;
		String str = null;
		String SQL = "INSERT INTO dbo.main " + 
				"VALUES (?,?,?,?,?,?,?,?,?,?)";
                //"VALUES ('TEST_WRITE',998, 'TEST_PROF', '8888888', '10:10:00','11:11:00',0,0,'12/8/2003')";
		
		try {
			preparedStatement = con.prepareStatement(SQL);
			
			preparedStatement.setString(1, event.getCourseName());
			preparedStatement.setLong(2, event.getId());
			preparedStatement.setString(3, event.getProfessor());
			preparedStatement.setString(4,event.getRoom());
			//for signature
			Time x = new Time(event.getStartLong());
			preparedStatement.setLong(5, event.getStartLong() );
			preparedStatement.setLong(6, event.getEndLong());
			preparedStatement.setInt(7, event.getStatus().getNumVal());
			preparedStatement.setBoolean(8, event.isOfflineFileNeeded());
			preparedStatement.setDate(9, event.getDate());
			str = event.getDate().toString() +"_"+ x.toString()+ "_"+event.getCourseName();
			preparedStatement.setString(10, str);
			
			//System.out.println("SIGNATURE IS:" + str);
			
	
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//System.out.println("PRIMARY KEY ALREADY IN DB");
			//e.printStackTrace();
		}

		return;
	}
	
	public void updateEventStatus(ClassroomEvent event) {
		//Take the event in and update the status.
		//Use what status is inside event, not a hard coded one
		System.out.println("Test: Updated. IMPLEMENT ME.");
		
		Time x = new Time(event.getStartLong());
		String signature = event.getDate().toString() +"_"+ x.toString()+ "_"+event.getCourseName();
		
		
		String sql = "UPDATE dbo.main SET Status = '" +event.getStatus().getNumVal()+
                "' WHERE Signature = '" +signature+ "' ";
		
		try {
			int tmp = stmt.executeUpdate(sql);
					
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return;
		
		
	}
	
	public void updateEventsStatus(ArrayList<ClassroomEvent> events) {
		for(ClassroomEvent e : events) {
			updateEventStatus(e);
		}
		return;
	}
	
	
}

