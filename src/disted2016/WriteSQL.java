package disted2016;


import java.sql.*;

public class WriteSQL {

   public static void main(String[] args) {

     
      String connectionUrl = "jdbc:sqlserver://129.138.23.50:1433;" +
         "databaseName=classviewdb;user=sa;password=password";

     
      Connection con = null;
      Statement stmt = null;
      	ResultSet rs = null;

      try {
         
         Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
         con = DriverManager.getConnection(connectionUrl);

         
         System.out.println("Inserting records into the table...");
         
 
       
         String SQL = "INSERT INTO dbo.main " + 
                 "VALUES ('TEST_WRITE',998, 'TEST_PROF', '8888888', '10:10:00','11:11:00',0,0,'12/8/2003')";
         stmt = con.createStatement();
         stmt.executeUpdate(SQL);
         con.commit(); // ADDED
         
         
         
      }

     
      catch (Exception e) {
         e.printStackTrace();
      }
      finally {
         if (rs != null) try { rs.close(); } catch(Exception e) {}
         if (stmt != null) try { stmt.close(); } catch(Exception e) {}
         if (con != null) try { con.close(); } catch(Exception e) {}
      }
   }
}