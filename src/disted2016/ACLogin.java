package disted2016;

import java.io.*;
import java.net.*;

public class ACLogin {

	private static ACLogin instance = null;
	
	private static String cookie = new String("");
	private static Boolean notifier = false;
	
	public static ACLogin getInstance() {
		if(instance == null) {
			instance = new ACLogin();
		}
		
		return instance;
	}
	
	private ACLogin()
	{
		;
	}

	public static String getCookie() {
		return cookie;
	}
	
	public static void logout() {
		cookie = null;
	}
	
	public static boolean login(String user, String pass) throws MalformedURLException, IOException
    {
        String url = "http://breeze.nmt.edu/api/xml?action=login&login=" + user + "&password=" + pass;
        URL realUrl = new URL(url);
        URLConnection conn = realUrl.openConnection();
        ACLogin.setCookie(conn.getHeaderField("set-cookie"));
        
        String[] temp = cookie.split(";");
        temp = temp[0].split("=");
        
        cookie = temp[1];
        
        if(cookie == null) {
        	cookie = "";
            return false;
        }
        
        return true;
        
    }

	private static void setCookie(String cookie) {
		ACLogin.cookie = cookie;
	}

	public static Boolean getNotifier() {
		return notifier;
	}

	public static void setNotifier(Boolean notifier) {
		ACLogin.notifier = notifier;
	}

}
